const express = require("express");
const app = express();

app.set("view engine","ejs");
app.use(express.static("public"));

// Adicionando o SQLITE na aplicação
const sqlite = require('sqlite');

// Conectando na base de dados
const dbConnection = sqlite.open('banco.sqlite',{Promise});

app.get("/",async(request,response) =>{

    const db = await dbConnection;
    const categoriaDB = await db.all('select * from categorias');
    const categorias = categoriaDB.map(cat => {
        return{
            ...cat
        }
    })

    response.render("home",{
        categorias
    });

});


app.get("/vaga",async(request,response)=>{
    const db = await dbConnection;
    const vagas = await db.all('select * from vagas');
    response.render("vagas",{
        vagas
    })
});

app.get("/contato",function(request,response){
    response.render("contato")
});

app.listen(3000,(error)=>{
    if(error){
        console.log("Não foi possível conectar");
    }
    else
    {
        console.log("App rodando.");
    }
});

const init = async() =>{
    const db = await dbConnection;
    await db.run('create table if not exists categorias (id INTEGER PRIMARY KEY,categoria TEXT);');
    await db.run('create table if not exists vagas (id INTEGER PRIMARY KEY,categoria INTEGER,titulo TEXT,descricao TEXT);');
    const vaga = "Full Stack Developer";
    const descricao = 'Isto é uma descrição';
    await db.run(`insert into vagas(categoria,titulo,descricao) values (1,'${vaga}','${descricao}')`);

}

init();